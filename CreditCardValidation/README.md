CreditCardValidation
====================

This directory contains sample projects for Xamarin Test Cloud. There are three directories:

* `CreditCardValidation-Start` - This folder holds the sample project, without any tests at all.
* `CreditCardValidation-UITest` -  This folder holds the sample project with tests written in C# using Xamarin.UITest. It is the companion code for the [Introduction to Xamarin.UITest](http://developer.xamarin.com/guides/testcloud/uitest/intro-to-uitest/) guide.
* `CreditCardValidation-Calabash` - This folder holds the sample project with tests written in Ruby using Calabash. [Calabash test documents](http://developer.xamarin.com/guides/testcloud/uitest/intro-to-uitest/)
